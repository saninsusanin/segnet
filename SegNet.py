class SegNet(nn.Module):
    def __init__(self):
        super().__init__()

        # encoder (downsampling)
        self.enc_conv0 = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3, bias=False, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.pool0 = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)

        self.enc_conv1 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, bias=False, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU()
        )
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)

        self.enc_conv2 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, bias=False, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, bias=False, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU()
        )
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)

        self.enc_conv3 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, bias=False, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, bias=False, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU()
        )
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)

        self.bottleneck = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=1, bias=False), 
            nn.BatchNorm2d(512), 
            nn.ReLU(), 
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=1, bias=False), 
            nn.BatchNorm2d(512), 
            nn.ReLU())

        # decoder (upsampling)
        self.unpool3 = nn.MaxUnpool2d(kernel_size=2, stride=2)
        self.dec_conv3 = nn.Sequential(nn.ConvTranspose2d(in_channels=512, 
                                                          out_channels=512, 
                                                          kernel_size=3, 
                                                          padding=1, 
                                                          bias=False), 
                                       nn.BatchNorm2d(512),
                                       nn.ReLU(),
                                       nn.ConvTranspose2d(in_channels=512, 
                                                          out_channels=256, 
                                                          kernel_size=3, 
                                                          padding=1,
                                                          bias=False), 
                                       nn.BatchNorm2d(256),
                                       nn.ReLU())
        self.unpool2 = nn.MaxUnpool2d(kernel_size=2, stride=2)
        self.dec_conv2 = nn.Sequential(nn.ConvTranspose2d(in_channels=256, 
                                                          out_channels=256, 
                                                          kernel_size=3, 
                                                          padding=1,
                                                          bias=False), 
                                       nn.BatchNorm2d(256),
                                       nn.ReLU(),
                                       nn.ConvTranspose2d(in_channels=256, 
                                                          out_channels=128, 
                                                          kernel_size=3, 
                                                          padding=1,
                                                          bias=False), 
                                       nn.BatchNorm2d(128),
                                       nn.ReLU())
        self.unpool1 = nn.MaxUnpool2d(kernel_size=2, stride=2)
        self.dec_conv1 = nn.Sequential(nn.ConvTranspose2d(in_channels=128, 
                                                          out_channels=64, 
                                                          kernel_size=3, 
                                                          padding=1, 
                                                          bias=False), 
                                       nn.BatchNorm2d(64),
                                       nn.ReLU())
        self.unpool0 = nn.MaxUnpool2d(kernel_size=2, stride=2)
        self.dec_conv0 = nn.Sequential(nn.ConvTranspose2d(in_channels=64, 
                                                          out_channels=1, 
                                                          kernel_size=3, 
                                                          padding=1, 
                                                          bias=False), 
                                       )

    def forward(self, x):
        # encoder
        x, indices0 = self.pool0(self.enc_conv0(x))
        x, indices1 = self.pool1(self.enc_conv1(x))
        x, indices2 = self.pool2(self.enc_conv2(x))
        x, indices3 = self.pool3(self.enc_conv3(x))

        # bottleneck
        x = self.bottleneck(x)

        # decoder
        x = self.dec_conv3(self.unpool3(x, indices3))
        x = self.dec_conv2(self.unpool2(x, indices2))
        x = self.dec_conv1(self.unpool1(x, indices1))
        x = self.dec_conv0(self.unpool0(x, indices0))

        return x
